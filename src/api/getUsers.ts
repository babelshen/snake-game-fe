import axios from 'axios';

export const getAllUsers = () => axios
    .get('https://snake-game-qtbj.onrender.com/api/v1/users')
    .then((res) => res.data)
    .catch(() => {
      throw new Error('Cannot load list of top-players');
    });
