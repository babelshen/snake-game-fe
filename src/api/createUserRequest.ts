import axios from 'axios';

export const createUserRequest = async (value: string, score: number) => {
  await axios({
    method: 'post',
    url: 'https://snake-game-qtbj.onrender.com/api/v1/users',
    data: {
      name: value,
      score: Number(score),
    },
  }).catch(() => {
    throw new Error('Failed to save the data on the new record');
  });
};
