export const pressKeyHandler = (direction: string, setDirection: (param: string) => void) => (e: KeyboardEvent) => {
  switch (e.key) {
    case 'ArrowRight':
      if (direction !== 'left') setDirection('right');
      else setDirection('left');
      break;
    case 'ArrowLeft':
      if (direction !== 'right') setDirection('left');
      else setDirection('right');
      break;
    case 'ArrowUp':
      if (direction !== 'down') setDirection('up');
      else setDirection('down');
      break;
    case 'ArrowDown':
      if (direction !== 'up') setDirection('down');
      else setDirection('up');
      break;
    default:
      break;
  }
};
