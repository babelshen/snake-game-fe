import { PLAYGROUND_LENGTH } from './globalVariables';

export function generateFood() {
  const x = Math.floor(Math.random() * PLAYGROUND_LENGTH);
  const y = Math.floor(Math.random() * PLAYGROUND_LENGTH);
  return { x, y };
}
