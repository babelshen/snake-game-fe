import { ISnake } from 'components/Playground/GameProcess/Game/interface';
import { PLAYGROUND_LENGTH } from './globalVariables';

export const isSnakeCheck = (element: ISnake, index: number) => {
  const x = index % PLAYGROUND_LENGTH;
  const y = Math.floor(index / PLAYGROUND_LENGTH);
  return element.x === x && element.y === y;
};
