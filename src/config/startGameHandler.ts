import { ISnake } from 'components/Playground/GameProcess/Game/interface';
import { generateFood } from './generateFood';

export const startGameHandler =
  (
    value: string,
    setError: (param: string) => void,
    setGameOver: (param: boolean) => void,
    setScore: (param: number) => void,
    setFood: (param: { x: number; y: number }) => void,
    setSnake: (param: ISnake[]) => void,
    setDirection: (param: string) => void,
    setStartGame: (param: boolean) => void,
    setSpeed: (param: number) => void,
  ) =>
  () => {
    if (value.length < 3) {
      setError('The name must contain a minimum of three characters');
    } else if (value.length > 15) {
      setError('The name should not be more than fifteen characters');
    } else {
      setGameOver(false);
      setScore(1);
      setFood(generateFood());
      setSnake([
        { x: 1, y: 0 },
        { x: 0, y: 0 },
      ]);
      setDirection('right');
      setSpeed(500);
      setStartGame(true);
    }
  };
