import { ISnake } from 'components/Playground/GameProcess/Game/interface';

export const snakeMoveHandler = (snake: ISnake[], direction: string, setSnake: (param: ISnake[]) => void) => () => {
  const newSnake = [...snake];
  const snakeHead = { ...newSnake[0] };

  switch (direction) {
    case 'right':
      snakeHead.x + 1 > 9 ? (snakeHead.x = 0) : (snakeHead.x += 1);
      break;
    case 'left':
      snakeHead.x - 1 < 0 ? (snakeHead.x = 9) : (snakeHead.x -= 1);
      break;
    case 'up':
      snakeHead.y - 1 < 0 ? (snakeHead.y = 9) : (snakeHead.y -= 1);
      break;
    case 'down':
      snakeHead.y + 1 > 9 ? (snakeHead.y = 0) : (snakeHead.y += 1);
      break;
    default:
      break;
  }
  newSnake.unshift(snakeHead);
  if (newSnake.length > 1) {
    newSnake.pop();
  }
  setSnake(newSnake);
};
