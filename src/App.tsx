import Footer from 'components/Footer/Footer';
import Header from 'components/Header';
import Playground from 'components/Playground';

const App = () => (
    <div className="wrapper">
      <Header />
      <main className="main">
        <Playground />
      </main>
      <Footer />
    </div>
  );

export default App;
