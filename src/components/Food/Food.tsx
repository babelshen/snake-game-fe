import style from './Food.module.scss';

const Food: React.FC<{ x: number; y: number }> = ({ x, y }) => <div className={style.food} style={{ left: `${x * 32}px`, top: `${y * 32}px` }} />;

export default Food;
