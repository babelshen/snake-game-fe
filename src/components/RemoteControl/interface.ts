export interface IMobileRemoteControl {
  direction: string;
  setDirection: (param: string) => void;
}
