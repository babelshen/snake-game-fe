import MobileRemoteControlButton from 'components/UI/MobileRemoteControlButton';
import style from './RemoteControl.module.scss';
import { IMobileRemoteControl } from './interface';

const MobileRemoteControl: React.FC<IMobileRemoteControl> = ({ direction, setDirection }) => {
  const setDirectionHandler = (data: string) => {
    switch (data) {
      case 'ArrowRight':
        if (direction !== 'left') setDirection('right');
        else setDirection('left');
        break;
      case 'ArrowLeft':
        if (direction !== 'right') setDirection('left');
        else setDirection('right');
        break;
      case 'ArrowUp':
        if (direction !== 'down') setDirection('up');
        else setDirection('down');
        break;
      case 'ArrowDown':
        if (direction !== 'up') setDirection('down');
        else setDirection('up');
        break;
      default:
        break;
    }
  };

  return (
    <div className={style.controller}>
      <MobileRemoteControlButton onClick={() => setDirectionHandler('ArrowUp')}>Up</MobileRemoteControlButton>
      <div className={style.controller__button_side}>
        <MobileRemoteControlButton onClick={() => setDirectionHandler('ArrowLeft')}>Left</MobileRemoteControlButton>
        <MobileRemoteControlButton onClick={() => setDirectionHandler('ArrowRight')}>Right</MobileRemoteControlButton>
      </div>
      <MobileRemoteControlButton onClick={() => setDirectionHandler('ArrowDown')}>Down</MobileRemoteControlButton>
    </div>
  );
};

export default MobileRemoteControl;
