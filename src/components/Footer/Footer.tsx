import style from './Footer.module.scss';

const Footer = () => (
    <footer className={style.footer}>
      <div className={style.footer__wrapper}>
        <h3 className={style.footer__text}>Made by Illia Babelnyk</h3>
      </div>
    </footer>
  );

export default Footer;
