import style from './Header.module.scss';

const Header = () => (
    <header className={style.header}>
      <div className={style.header__wrapper}>
        <h1 className={style.header__text}>Snake game</h1>
      </div>
    </header>
  );

export default Header;
