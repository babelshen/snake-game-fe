import { ReactNode } from 'react';

export interface IMobileRemoteControlButton {
  children: ReactNode;
  onClick: () => void;
}
