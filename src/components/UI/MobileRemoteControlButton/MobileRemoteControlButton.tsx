import { IMobileRemoteControlButton } from './interface';
import style from './MobileRemoteControlButton.module.scss';

const MobileRemoteControlButton: React.FC<IMobileRemoteControlButton> = ({ children, onClick }) => (
    <button type="button" onClick={onClick} className={style.mobile__button}>
      {children}
    </button>
  );

export default MobileRemoteControlButton;
