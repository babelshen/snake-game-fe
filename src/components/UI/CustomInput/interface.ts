import { ChangeEvent } from 'react';

export interface ICustomInput {
  placeholder: string;
  value: string;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}
