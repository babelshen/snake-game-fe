import { ICustomInput } from './interface';
import style from './CustomInput.module.scss';

const CustomInput: React.FC<ICustomInput> = ({ placeholder, value, onChange }) => <input type="text" placeholder={placeholder} value={value} onChange={onChange} className={style.input} />;

export default CustomInput;
