import { ReactNode } from 'react';

export interface ICustomButton {
  children: ReactNode;
  type: 'submit' | 'button';
  onClick: () => void;
}
