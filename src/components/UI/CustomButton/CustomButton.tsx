import style from './CustomButton.module.scss';
import { ICustomButton } from './interface';

const CustomButton: React.FC<ICustomButton> = ({ children, type, onClick }) => (
    <button type={type} className={style.button} onClick={onClick}>
      {children}
    </button>
  );

export default CustomButton;
