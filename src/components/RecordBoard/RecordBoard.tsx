import { useEffect, useState } from 'react';
import { getAllUsers } from 'api/getUsers';
import { IRecordBoard, IUsers } from './interface';
import style from './RecordBoard.module.scss';

const RecordBoard: React.FC<IRecordBoard> = ({ gameOver }) => {
  const [users, setUsers] = useState<IUsers[]>([]);
  const [error, setError] = useState<null | Error>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await getAllUsers();
        setUsers(result);
      } catch (e) {
        setError(e as Error);
      }
    };
    fetchData();
  }, [gameOver, error]);

  return (
    <section className={style.record__wrapper}>
      <h2 className={style.record__title}>Top 5 players</h2>
      <ul className={style.record__list}>
        {error ? (
          <p>{error.message}</p>
        ) : users.length > 0 ? (
          users.map((user) => (
            <li key={user.id} className={style.record__item}>
              <span>{user.name}</span>
              <span>{user.score}</span>
            </li>
          ))
        ) : (
          <p>Nobody</p>
        )}
      </ul>
    </section>
  );
};

export default RecordBoard;
