export interface IRecordBoard {
  gameOver: boolean;
}

export interface IUsers {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  name: string;
  score: number;
}
