export interface IStartGame {
  startGameHandler: () => void;
  error: string;
  value: string;
  setValue: (param: string) => void;
}
