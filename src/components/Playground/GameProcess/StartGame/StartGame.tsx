import { ChangeEvent } from 'react';
import CustomButton from 'components/UI/CustomButton';
import CustomInput from 'components/UI/CustomInput';
import style from './StartGame.module.scss';
import { IStartGame } from './interface';

const StartGame: React.FC<IStartGame> = ({ startGameHandler, error, value, setValue }) => (
    <section className={style.game__info}>
      <h2 className={style.game__info_text}>Input your name and press start to play</h2>
      <CustomInput value={value} onChange={(e: ChangeEvent<HTMLInputElement>) => setValue(e.target!.value)} placeholder="Your name" />
      {error ? <p className={style.game__info_error}>{error}</p> : false}
      <CustomButton type="button" onClick={startGameHandler}>
        Start
      </CustomButton>
    </section>
  );

export default StartGame;
