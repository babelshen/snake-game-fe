export interface IEndGame {
  startGameHandler: () => void;
  score: number;
}
