import CustomButton from 'components/UI/CustomButton';
import { IEndGame } from './interface';
import style from './EndGame.module.scss';

const EndGame: React.FC<IEndGame> = ({ startGameHandler, score }) => (
    <section className={style.game__info}>
      <h2 className={style.game__info_text}>Game over</h2>
      <p className={style.game__info_text}>Your score: {score}</p>
      <CustomButton type="button" onClick={startGameHandler}>
        Try again
      </CustomButton>
    </section>
  );

export default EndGame;
