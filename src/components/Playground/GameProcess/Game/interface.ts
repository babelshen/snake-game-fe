export interface ISnake {
  x: number;
  y: number;
}

export interface IGame {
  score: number;
  food: { x: number; y: number };
  snake: ISnake[];
  direction: string;
  setDirection: (param: string) => void;
  pause: boolean;
  setPause: (param: boolean) => void;
}
