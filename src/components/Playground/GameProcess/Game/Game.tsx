import { PLAYGROUND_LENGTH } from 'config/globalVariables';
import { isSnakeCheck } from 'config/isSnakeCheck';
import Food from 'components/Food';
import MobileRemoteControl from 'components/RemoteControl';
import Snake from 'components/Snake';
import CustomButton from 'components/UI/CustomButton';
import style from './Game.module.scss';
import { IGame, ISnake } from './interface';

const Game: React.FC<IGame> = ({ score, food, snake, direction, setDirection, pause, setPause }) => (
    <section className={style.playground__wrapper}>
      <h2 className={style.playground__score}>Score: {score}</h2>
      <div className={style.playground}>
        <Food x={food.x} y={food.y} />
        {Array.from({ length: PLAYGROUND_LENGTH * PLAYGROUND_LENGTH }, (_, i) => (
          <div className={style.playground__item} key={i}>
            {snake.some((element: ISnake) => isSnakeCheck(element, i)) && <Snake />}
          </div>
        ))}
      </div>
      <CustomButton type="button" onClick={() => setPause(!pause)}>
        {pause ? 'Restore' : 'Pause'}
      </CustomButton>
      {innerWidth < 900 ? <MobileRemoteControl direction={direction} setDirection={setDirection} /> : false}
    </section>
  );

export default Game;
