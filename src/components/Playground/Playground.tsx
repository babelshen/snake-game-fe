import { useEffect, useState } from 'react';
import { createUserRequest } from 'api/createUserRequest';
import { generateFood } from 'config/generateFood';
import { pressKeyHandler } from 'config/pressKeyHandler';
import { snakeMoveHandler } from 'config/snakeMoveHandler';
import { startGameHandler } from 'config/startGameHandler';
import RecordBoard from 'components/RecordBoard';
import Game from './GameProcess/Game';
import EndGame from './GameProcess/EndGame';
import StartGame from './GameProcess/StartGame';
import style from './Playground.module.scss';

const Playground = () => {
  const [snake, setSnake] = useState([
    { x: 1, y: 0 },
    { x: 0, y: 0 },
  ]);
  const [direction, setDirection] = useState('right');
  const [food, setFood] = useState(generateFood());
  const [gameOver, setGameOver] = useState(false);
  const [score, setScore] = useState(1);
  const [startGame, setStartGame] = useState(false);
  const [value, setValue] = useState('');
  const [error, setError] = useState('');
  const [pause, setPause] = useState(false);
  const [speed, setSpeed] = useState(500);

  const handleStartGame = startGameHandler(value, setError, setGameOver, setScore, setFood, setSnake, setDirection, setStartGame, setSpeed);
  const handlePressKey = pressKeyHandler(direction, setDirection);
  const handleSnakeMove = snakeMoveHandler(snake, direction, setSnake);

  useEffect(() => {
    if (score % 50 === 0) setSpeed(Math.floor(score / 50) * 50);
    if (!pause) {
      const moveInterval = setInterval(handleSnakeMove, speed);
      return () => {
        clearInterval(moveInterval);
      };
    } 
  }, [handleSnakeMove, score, pause, speed]);

  useEffect(() => {
    document.addEventListener('keydown', handlePressKey);
    return () => {
      document.removeEventListener('keydown', handlePressKey);
    };
  }, [handlePressKey]);

  useEffect(() => {
    if (snake[0].x === food.x && snake[0].y === food.y) {
      setFood(generateFood());
      const newSnake = [...snake];
      const tail = { ...newSnake[newSnake.length - 1] };
      newSnake.push(tail);
      setSnake(newSnake);
      setScore((prev) => (prev === 1 ? (prev += 4) : (prev += 5)));
    }
    for (let i = 1; i < snake.length; i++) {
      if (snake[0].x === snake[i].x && snake[0].y === snake[i].y) {
        setGameOver(true);
      }
    }
  }, [food, snake]);

  useEffect(() => {
    if (gameOver && score > 5) {
      createUserRequest(value, score).catch((error) => console.log(error));
    }
  }, [gameOver, value, score]);

  return (
    <div className={style.wrapper}>
      <div className={style.game}>
        {startGame ? (
          gameOver ? (
            <EndGame score={score} startGameHandler={handleStartGame} />
          ) : (
            <Game score={score} food={food} snake={snake} direction={direction} setDirection={setDirection} pause={pause} setPause={setPause} />
          )
        ) : (
          <StartGame startGameHandler={handleStartGame} error={error} value={value} setValue={setValue} />
        )}
      </div>
      <RecordBoard gameOver={gameOver} />
    </div>
  );
};

export default Playground;
